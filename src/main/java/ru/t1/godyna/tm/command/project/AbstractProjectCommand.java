package ru.t1.godyna.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.godyna.tm.api.service.IProjectService;
import ru.t1.godyna.tm.api.service.IProjectTaskService;
import ru.t1.godyna.tm.command.AbstractCommand;
import ru.t1.godyna.tm.enumerated.Role;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected IProjectService getProjectService() {
        return serviceLocator.getProjectService();
    }

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
